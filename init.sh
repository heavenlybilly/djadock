RED='\033[0;31m'
GREEN='\033[0;32m'
NORMAL='\033[0m'
YELLOW='\033[33m'

# files
echo "${GREEN}Preparing${NORMAL}"

rm -rf .git
rm -rf .env
cp .env.example .env
sudo chmod 777 .env
mkdir project